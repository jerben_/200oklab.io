---
title: Coworking in Glarus
authors: Alain M. Lafon
category: coworking
date-created: 2017-10-19
tags: glarus
uuid: a8d0756c-88fa-4a80-861a-c560cec69b9d
---

Happy to be coworking with our friends from
[ungleich glarus ag](https://ungleich.ch/) in Linthal, Glarus on our
new joint venture: Glarner Crowdfunding AG.

As an added bonus, we have the best working conditions we could ever
ask for.

Come and join us any time for a coffee, a tea or a coworking session
in our offices in Glarus or Schwanden.

![](/img/2017-10-19/IMG_3372.jpg)
![](/img/2017-10-19/IMG_3373.jpg)
