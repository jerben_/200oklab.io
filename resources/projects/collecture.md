---
name: "collecture"
image: "collecture_lockup.svg"
position: 1
uuid: 35a6e28a-b1dd-4ff3-a897-a0e7280572f4
---

**The place to record and listen to lectures**

Record and listen to lectures on
[collecture.io](https://collecture.io) - on the go, when you need it
and at your own pace.

**Give students the ability to**

- Stop time
- Go back to every lecture
- Learn at their own pace

**Record lectures with less than 30s effort**

**Bring your own device**

**Share recordings**

- Web access
- Share only a link
- No need to install anything

**Public and private sharing**

**As a lecturer, prepare your next lecture by listening to your colleagues**

Collecture is in the catalog of software for lecturers at
[ZHAW](http://zhaw.ch) and used in various lectures at the School of
Engineering and the School of Management and Law.

Visit [collecture.io](https://collecture.io) or download the
[iOS](https://itunes.apple.com/ch/app/collecture/id1185222314?l=en&mt=8)
or
[Android](https://play.google.com/store/apps/details?id=io.collecture) App.
