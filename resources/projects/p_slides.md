---
name: "p_slides"
image: "p_slides.png"
position: 2
uuid: 7bee0200-1244-4e80-85be-c222bdec2c85
---

**Simple semantic slides**

[p_slides](https://github.com/munen/p_slides) is open source under the
AGPLv3 license.

- Only think about content, don't worry about styling 
- Static files only, no need for a server, installed software,
  pre-compilation or a special editor (unless of course your favorite
  and very special editor!)
- For editing and presenting, there are no external dependencies -
  it's just a html file (therefore it can easily be put into version
  control for example)
- Extendable and supporting themes
- Write slides in markdown

p_slides is used in various courses at [ZHAW](zhaw.ch) as well as
other people.

You can find it on [Github](https://github.com/munen/p_slides).
