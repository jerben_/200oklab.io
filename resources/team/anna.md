---
name: Anna Kälin
email: annaflurina@200ok.ch
image: /img/anna.jpg
position: 60
uuid: 50403e2f-5da2-4e11-98ad-686b25889432
---

### Anna-Flurina Kälin

Anna has a Master of Arts in Art Education and is currently working on
her degree in Computer Science. She works as a lecturer and designer.
At 200ok, she combines her inner artist with her love for programming.
