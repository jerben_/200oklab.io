---
name: Savino Jossi
email: savino@200ok.ch
image: /img/savino.jpg
position: 80
uuid: 107750aa-afb0-4e9e-aaec-cb50bce98d37
---

### Savino Jossi, Intern

Savino is a student at ZHAW where he met Alain as a lecturer. At 200ok
he is honing his Clojure and ClojureScript skills.
