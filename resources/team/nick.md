---
name: Nick Niles
email: nick@200ok.ch
image: /img/nick.jpg
position: 65
uuid: 50403e2f-5da2-4e11-98ad-686b25889432
---

### Nick Niles

Nick has been designing for the web since 1997. At 200ok, he combines
his love for both design and front-end development.
