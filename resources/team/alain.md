---
name: Alain M. Lafon
image: /img/alain.jpg
email: alain@200ok.ch
position: 10
uuid: a3948985-c326-4f1b-b2f5-521d91a01808
---

### Alain M. Lafon, Founder & CEO

Alain has been programming for the web since 2004. He is an avid
follower of Web Standards and constantly improving his own
productivity as well as the productivity of the team he is working
with by his profound knowledge about agile methodologies as well as
tooling to facilitate a smooth software development process.

He is also a lecturer at [ZHAW](http://zhaw.ch) and an ordained monk
tending to a
[Zen Temple](http://zen-temple.net/zen-temples/lambda-zen-temple/introduction/)
in Glarus, Switzerland.
