---
name: Dibran Isufi
email: dibran@200ok.ch
image: /img/dibran.jpg
position: 40
uuid: 28f71257-ead7-4599-b6fd-454b6abc01f3
---

### Dibran Isufi, Code Gluer

Glues all the code together.
