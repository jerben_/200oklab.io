---
name: Phil Hofmann
email: phil@200ok.ch
image: /img/phil.jpg
position: 20
uuid: 769734a1-bc69-43fe-9be9-73d5a319a9de
---

### Phil Hofmann, Founder

Phil has been programming for the web since 1999. He is an experienced
Software Engineer.
