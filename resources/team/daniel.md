---
name: Daniel Einars
email: daniel@200ok.ch
image: /img/daniel.jpg
position: 70
uuid: bb581b55-c1c6-4685-bb46-a25916fbd1ad
---

### Daniel Einars, Intern

Daniel is a student at ZHAW where he met Alain as a lecturer. His
original education is in marketing. At 200ok he puts that to good use
while learning Clojure and ClojureScript.
