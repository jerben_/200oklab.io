(set-env!
 :source-paths #{"src"}
 :resource-paths #{"resources" "assets"}
 :dependencies '[[org.clojure/clojure "1.8.0"]
                 [perun "0.4.2-20170215.172730-2"]
                 [hiccup "1.0.5"]
                 [garden "1.3.2"]
                 [pandeiro/boot-http "0.7.3"]
                 [jeluard/boot-notify "0.1.2" :scope "test"]
                 [cpmcdaniel/boot-copy "1.0"]
                 [clj-http "2.3.0"]
                 [org.clojure/data.json "0.2.6"]
                 [deraen/boot-sass "0.3.1"]])

(require '[io.perun :refer [markdown draft slug ttr word-count build-date gravatar collection assortment render inject-scripts sitemap atom-feed global-metadata]]
         '[io.perun.meta :as perun]
         '[ok.index :as index-view]
         '[ok.post :as post-view]
         '[pandeiro.boot-http :refer [serve]]
         '[garden.core :refer [css]]
         '[cpmcdaniel.boot-copy :refer :all]
         '[clojure.data.json]
         '[clj-http.client]
         '[deraen.boot-sass :refer [sass]])
;;'[jeluard.boot-notify :refer [notify]]

(deftask slack
  "Post `message` to slack."
  [u url URL str "The slack incoming webhook url"
   m message MESSAGE str "The slack message"]
  (with-post-wrap fileset
    (let [data {:username "GitLab CI"
                :text message
                :icon_emoji ":gitlabci:"}
          json (clojure.data.json/write-str data)]
      (clj-http.client/post url {:body json}))))

(task-options!
 slack {:url "https://hooks.slack.com/services/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"}
 copy {:output-dir "target/public"
       ;; TODO: Make this regexp more readable. It has three parts:
       ;;       google search console + favicon stuff + other assets.
       :matching   #{#"(google.*\.html|safari-pinned-tab\.svg|favicon\.ico|browserconfig\.xml|manifest\.json)|\.(css|js|png|jpg|svg)$"}})

(defn slug-fn
  "Slugs are derived from filenames of html files. They can have a
  YYYY-MM-DD- prefix or not."
  [_ {:keys [filename]}]
  (last (re-find #"(\d+-\d+-\d+-|)(.*)\.html" filename)))

(deftask set-meta-data
  "Add :key attribute with :val value to each file metadata and also
   to the global meta"
  [k key VAL kw "meta-data key"
   v val VAL str "meta-data value"]
  (with-pre-wrap fileset
    (let [files           (perun/get-meta fileset)
          global-meta     (perun/get-global-meta fileset)
          updated-files   (map #(assoc % key val) files)
          new-global-meta (assoc global-meta key val)
          updated-fs      (perun/set-meta fileset updated-files)]
      (perun/set-global-meta updated-fs new-global-meta))))

(deftask categories
  "Add :categories of all posts to the meta-data"
  []
  (with-pre-wrap fileset
    (let [files           (perun/get-meta fileset)
          global-meta     (perun/get-global-meta fileset)
          categories      (filter #(:category %) files)
          updated-files   (map #(assoc % :categories categories) files)
          new-global-meta (assoc global-meta :categories categories)
          updated-fs      (perun/set-meta fileset updated-files)]
      (perun/set-global-meta updated-fs new-global-meta))))

(defn category-grouper
  [entries]
  (reduce (fn [result entry]
            (let [category (:category entry)
                  path (str category ".html")]
              (-> result
                  (update-in [path :entries] conj entry)
                  (assoc-in [path :entry :category] category))))
          {}
          entries))

(deftask build
  "Build the 200ok page."
  []
  (let [is-of-type? (fn [{:keys [permalink]} doc-type] (.startsWith permalink (str "/" doc-type)))]
    (comp
     (markdown)
     (draft)
     ;;(print-meta)
     (slug :slug-fn slug-fn)
     (ttr)
     (categories)
     (word-count)
     (build-date)
     (gravatar :source-key :author-email
               :target-key :author-gravatar)

     (collection :renderer 'ok.index/render
                 :page "index.html"
                 ;; Order pages in reverse chronological order
                 :sortby #(:date-created %)
                 :comparator #(.compareTo %2 %1)
                 :filterer #(is-of-type? % "posts"))

     (collection :renderer 'ok.project/render-collection
                 :page "projects.html"
                 :filterer #(is-of-type? % "projects"))

     (collection :renderer 'ok.person/render-collection
                 :page "team.html"
                 :filterer #(is-of-type? % "team"))

     (collection :renderer 'ok.index/render
                 :page "open-source.html"
                 :filterer #(is-of-type? % "open-source"))

     ;; Groups all posts that have a :category (yes, only a single one
     ;; atm) into one file. For example a post with a :category of
     ;; "emacs" will be rendered into a file
     ;; "public/category/emacs.html" together with every other post
     ;; with the same tag.
     (assortment :renderer 'ok.index/render
                 :grouper category-grouper
                 :out-dir "public/category"
                 :filterer #(is-of-type? % "posts"))

     ;; renders each md file in posts into its own page
     (render :renderer 'ok.post/render
             :filterer #(is-of-type? % "posts"))

     (render :renderer 'ok.page/render
             :filterer #(is-of-type? % "audio-book"))

     (inject-scripts :scripts #{"start.js"})
     (sitemap)
     (atom-feed :filterer #(is-of-type? % "posts"))
     ;;(notify)
     ;;(print-meta)
     (target)
     (copy))))

(deftask dev
  []
  (comp
   (watch)
   (sass)
   (global-metadata)
   (set-meta-data :key :target
                  :val "dev")
   (build)
   (serve :dir "target/public")))

(deftask prod
  []
  (comp
   ;;(slack :message "<http://200ok.ch|200ok.ch> has been updated.")
   (sass)
   (global-metadata)
   (set-meta-data :key :target
                  :val "prod")
   (build)))
