(ns ok.shared
  (:use [hiccup.core :only (html)]
        [hiccup.page :only (html5)]))


(defn render-footer []
  [:footer
   [:div.row.small-up-2.medium-up-4
    {:itemscope true
     :itemprop "publisher"
     :itemtype "https://schema.org/Organization"}
    [:div.columns{:itemprop "name"}
                  "200ok GmbH"]
    [:div.columns{:itemprop "address"
                  :itemscope true
                  :itemtype "https://schema.org/PostalAddress"}
     [:div.row
      [:div.columns{:itemprop "streetAddress"}
       "Badenerstrasse 313"]
      [:div.columns
       [:span{:itemprop "postalCode"}
        "8003"]
       " "
       [:span{:itemprop "addressLocality"}
        "Zürich"]]]]
    [:div.columns{:itemprop "telephone"}
                  "+41 76 405 05 67"]
    [:div.columns{:itemprop "email"}
     [:a {:href "mailto:info@200ok.ch"}
      "info@200ok.ch"]]]])
